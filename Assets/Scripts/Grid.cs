using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public static Grid Instance;
    public bool displayGridGizmos;
    public Transform player; //for debugging if player collides with nodes
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    public float GetNodeDiameter => nodeRadius * 2;
    public int MaxSize => gridSizeX * gridSizeY;
    Node[,] grid;
    List<Node> neighbours;
    float nodeDiameter, percentX, percentY;
    int gridSizeX, gridSizeY, checkX, checkY;
    Vector3 worldPoint;
    bool walkable;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        CreateGrid();
    }
    void CreateGrid()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        grid = new Node[gridSizeX, gridSizeY];
        for(int x = 0; x < gridSizeX; x++)
        {
            for(int y = 0; y < gridSizeY; y++)
            {
                worldPoint = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2 + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }
    public List<Node> GetNeighbours(Node node)
    {
        neighbours = new List<Node>();
        for(int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if(x == 0 && y == 0)
                {
                    continue;
                }
                checkX = node.gridX + x;
                checkY = node.gridY + y;
                if(checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }
        return neighbours;
    }
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        percentX = Mathf.Clamp01((worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x);
        percentY = Mathf.Clamp01((worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y);
        return grid[Mathf.RoundToInt((gridSizeX-1)*percentX), Mathf.RoundToInt((gridSizeY - 1) * percentY)];
    }

    //public List<Node> path;

    void OnDrawGizmos()
    {
        
        Gizmos.DrawWireCube(transform.position, Vector3.up*.25f + Vector3.right * gridWorldSize.x + Vector3.forward * gridWorldSize.y);
        if (grid != null && displayGridGizmos)
        {
            for (int x = 0; x < gridSizeX; x++)
            {
                for (int y = 0; y < gridSizeY; y++)
                {
                    Gizmos.color = grid[x, y].walkable ? Color.white : Color.red;
                    Gizmos.DrawCube(grid[x, y].worldPosition, Vector3.one * (nodeDiameter - .1f));
                }
            }
        }
        /*
        if (onlyDisplayPathGizmos)
        {
            if (path != null)
            {
                for (int x = 0; x < path.Count; x++)
                {
                    Gizmos.color = Color.black;
                    Gizmos.DrawCube(path[x].worldPosition, Vector3.one * (nodeDiameter - .1f));
                }
            }
        }
        else {
            if (grid != null)
            {
                for (int x = 0; x < gridSizeX; x++)
                {
                    for (int y = 0; y < gridSizeY; y++)
                    {
                        Gizmos.color = grid[x, y].walkable ? Color.white : Color.red;
                        //for debugging if player collides with nodes
                        //
                        //if(NodeFromWorldPoint(player.position) == grid[x, y])
                        //{
                        //    Gizmos.color = Color.cyan;
                        //}
                        //
                        if (path != null)
                        {
                            if (path.Contains(grid[x, y]))
                            {
                                Gizmos.color = Color.black;
                            }
                        }
                        Gizmos.DrawCube(grid[x, y].worldPosition, Vector3.one * (nodeDiameter - .1f));
                    }
                }
            }
        }
        */
    }
}
