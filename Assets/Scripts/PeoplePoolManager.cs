using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeoplePoolManager : MonoBehaviour
{
    public static PeoplePoolManager Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    
    [SerializeField]
    Transform spawnPoints, peopleContainer;
    [SerializeField]
    List<Unit> unit;
    public List<Unit> Unit => unit;
    public float timeToSpawn = 5;
    float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= timeToSpawn)
        {
            timer = 0;
            timeToSpawn = Random.Range(5, 11);
            //Spawn People
            if(transform.childCount > 0)
            {
                transform.GetChild(0).transform.position = spawnPoints.GetChild(Random.Range(0, spawnPoints.childCount)).position;
                transform.GetChild(0).gameObject.SetActive(true);
                transform.GetChild(0).SetParent(peopleContainer); //para lang di pakalat kalat sa hierarchy
            }
        }
    }
}
