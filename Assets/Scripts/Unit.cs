using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField]
    CapsuleCollider capsuleCollider;
    float speed = 25;
    Vector3[] path;
    Vector3 currentWayPoint, chosenRack, doorPosition, currentRack, exitPoint;
    float height;
    int targetIndex, randomRackNumber, currentRackIndex, generateRandomSign, randomSign;
    bool enteredShopFront, generatedFirstPath, doneShopping, exitedShopFront, lookingAtRack, checkedUnitInFront;
    public bool LookingAtRack => lookingAtRack;
    List<Vector3> visitedRacks = new List<Vector3>();
    List<int> randomRackList = new List<int>();
    void OnEnable()
    {
        visitedRacks.Clear();
        randomRackList.Clear();
        checkedUnitInFront = false;
        enteredShopFront = false;
        generatedFirstPath = false;
        doneShopping = false;
        exitedShopFront = false;
        lookingAtRack = false;

        currentRackIndex = 0;
        height = capsuleCollider.height / 2;
        transform.position = Vector3.right * transform.position.x + Vector3.up * height + Vector3.forward * transform.position.z;
        if(transform.position.x < 0)
        {
            transform.eulerAngles = Vector3.zero + Vector3.up * 90;
        }
        else if(transform.position.x > 0)
        {
            transform.eulerAngles = Vector3.zero + Vector3.up * 270;
        }
        path = new Vector3[0];
        randomRackList.Add(Random.Range(0, RackManager.Instance.transform.childCount));
        while(randomRackList.Count < RackManager.Instance.transform.childCount)
        {
            randomRackNumber = Random.Range(0, RackManager.Instance.transform.childCount);
            if (randomRackList.Contains(randomRackNumber))
            {
                continue;
            }
            randomRackList.Add(randomRackNumber);
        }
        exitPoint = ShopIntersection.Instance.transform.GetChild(Random.Range(0, ShopIntersection.Instance.transform.childCount)).position;
    }
    void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * 2, Color.red);
        if (!generatedFirstPath)
        {
            transform.Translate(transform.InverseTransformDirection(transform.forward) * speed * Time.deltaTime);
        }
        if (doneShopping)
        {
            transform.Translate(transform.InverseTransformDirection(transform.forward) * speed * Time.deltaTime);
        }
    }
    IEnumerator FaceStore()
    {
        while(transform.eulerAngles != Vector3.zero)
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, Vector3.zero, .25f);
        yield return new WaitForSeconds(1);
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Shop Front"))
        {
            if (!enteredShopFront)
            {
                enteredShopFront = true;
                StartCoroutine(FaceStore());
            }
        }
        if(other.CompareTag("Shop Door"))
        {
            if (!generatedFirstPath)
            {
                generatedFirstPath = true;
                doorPosition = other.transform.position;
                currentRack = RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).position;
                generateRandomSign = Random.Range(0, 2);
                randomSign = generateRandomSign == 0 ? randomSign = 1 : randomSign = -1;
                chosenRack = RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).GetChild(0).position + RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).GetChild(0).transform.right * Random.Range(0,4)*Grid.Instance.GetNodeDiameter * randomSign;
                currentRackIndex++;
                visitedRacks.Add(chosenRack);
                PathRequestManager.RequestPath(transform.position, chosenRack, OnPathFound);
            }
        }
        if (other.CompareTag("Despawn"))
        {
            transform.SetParent(PeoplePoolManager.Instance.transform);
            gameObject.SetActive(false);
        }
    }
    public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = newPath;
            StopCoroutine(FollowPath());
            StartCoroutine(FollowPath());
        }
    }
    IEnumerator DoneTraversingPath()
    {
        if(Vector3.Distance(transform.position, chosenRack) > Grid.Instance.GetNodeDiameter)
        {
            checkedUnitInFront = true;
        }
        if (visitedRacks.Count <= RackManager.Instance.transform.childCount && !exitedShopFront && !checkedUnitInFront)
        {
            lookingAtRack = true;
            yield return new WaitForSeconds(Random.Range(3, 6)); // this time is how long the model is looking at racks
            lookingAtRack = false;
        }
        else
        {
            checkedUnitInFront = false;
            yield return null;
        }
        if (visitedRacks.Count < RackManager.Instance.transform.childCount)
        {
            currentRack = RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).position;
            chosenRack = RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).GetChild(0).position + RackManager.Instance.transform.GetChild(randomRackList[currentRackIndex]).GetChild(0).transform.right * Random.Range(0, 4) * Grid.Instance.GetNodeDiameter * randomSign;
            currentRackIndex++;
            visitedRacks.Add(chosenRack);
            PathRequestManager.RequestPath(transform.position, chosenRack, OnPathFound); // will go to another rack if di pa tapos puntahan lahat
        }
        else
        {
            if (!exitedShopFront)
            {
                PathRequestManager.RequestPath(transform.position, doorPosition, OnPathFound); // will go to the door position if tapos na puntahan lahat
                exitedShopFront = true;
            }
            else if(exitedShopFront)
            {
                StartCoroutine(GetOutOfStore());
            }
        }
    }
    IEnumerator GetOutOfStore()
    {
        while (!doneShopping)
        {
            if (!Physics.Raycast(transform.position, transform.forward, 2, 1 << gameObject.layer))
            {
                transform.LookAt(exitPoint);
                transform.eulerAngles = Vector3.up * transform.eulerAngles.y;
                transform.position = Vector3.MoveTowards(transform.position, exitPoint + Vector3.up *  height, speed * Time.deltaTime);
            }
            if(Vector3.Distance(transform.position, exitPoint) <= 1)
            {
                doneShopping = true;
                transform.eulerAngles = Random.Range(0, 2) == 0 ? Vector3.up * 90 : Vector3.up * 270;
                yield break;
            }
            yield return null;

        }
    }
    IEnumerator FollowPath()
    {
        currentWayPoint = path[0];
        currentWayPoint.y = currentWayPoint.y + height;
        transform.LookAt(currentWayPoint);
        transform.eulerAngles = Vector3.right * 0 + Vector3.up * transform.eulerAngles.y + Vector3.forward * 0;
        while (true)
        {
            if (transform.position == currentWayPoint)
            {
                targetIndex++;
                if (targetIndex >= path.Length)
                {
                    // will go here if done with path
                    targetIndex = 0;
                    path = new Vector3[0];
                    if (visitedRacks.Count < RackManager.Instance.transform.childCount)
                    {
                        transform.LookAt(currentRack);
                        transform.eulerAngles = Vector3.right * 0 + Vector3.up * transform.eulerAngles.y + Vector3.forward * 0;
                    }
                    StartCoroutine(DoneTraversingPath());
                    yield break;
                }
                currentWayPoint = path[targetIndex];
                currentWayPoint.y = currentWayPoint.y + height;
                transform.LookAt(currentWayPoint);
                transform.eulerAngles = Vector3.right * 0 + Vector3.up * transform.eulerAngles.y + Vector3.forward * 0;
            }

            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 2, 1 << gameObject.layer))
            {
                for (int x = 0; x < PeoplePoolManager.Instance.Unit.Count; x++)
                {
                    if (hit.collider.gameObject == PeoplePoolManager.Instance.Unit[x].gameObject)
                    {
                        if (PeoplePoolManager.Instance.Unit[x].LookingAtRack)
                        {
                            checkedUnitInFront = true;
                            StartCoroutine(DoneTraversingPath());
                            yield break;
                        }
                    }
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, currentWayPoint, speed * Time.deltaTime);
            }
            yield return null;
        }
    }
    
    public void OnDrawGizmos()
    {
        if (path != null)
        {
            for(int x = targetIndex; x < path.Length; x++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[x], Vector3.one);
                if(x == targetIndex)
                {
                    Gizmos.DrawLine(transform.position, path[x]);
                }
                else
                {
                    Gizmos.DrawLine(path[x - 1], path[x]);
                }
            }
        }
    }
}
