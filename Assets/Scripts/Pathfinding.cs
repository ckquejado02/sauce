using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

public class Pathfinding : MonoBehaviour
{
    //public Transform seeker, target;
    [SerializeField]
    Grid grid;
    Node currentNode, startNode, targetNode;
    Heap<Node> openSet;
    HashSet<Node> closedSet;
    List<Node> path;
    List<Vector3> tempWayPoints;
    int distanceX, distanceY, newMovementCostToNeighbour;
    Vector3[] wayPoints, newWayPoints;
    Vector2 directionOld, directionNew;
    bool pathSuccess;
    private void Start()
    {
        openSet = new Heap<Node>(grid.MaxSize);
        closedSet = new HashSet<Node>();
    }
    public void StartFindPath(Vector3 startPosition, Vector3 targetPosition)
    {
        StartCoroutine(FindPath(startPosition, targetPosition));
    }
    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        wayPoints = new Vector3[0];
        pathSuccess = false;
        startNode = grid.NodeFromWorldPoint(startPos);
        targetNode = grid.NodeFromWorldPoint(targetPos);
        if (startNode.walkable && targetNode.walkable)
        {
            openSet.Clear();
            closedSet.Clear();
            openSet.Add(startNode);
            while (openSet.Count > 0)
            {
                currentNode = openSet.RemoveFirst();
                closedSet.Add(currentNode);
                if (currentNode == targetNode)
                {
                    stopWatch.Stop();
                    UnityEngine.Debug.Log("PATH FOUND: " + stopWatch.ElapsedMilliseconds + " ms");
                    pathSuccess = true;
                    //RetracePath(startNode, targetNode);
                    break;
                }
                for (int x = 0; x < grid.GetNeighbours(currentNode).Count; x++)
                {
                    if (!grid.GetNeighbours(currentNode)[x].walkable || closedSet.Contains(grid.GetNeighbours(currentNode)[x]))
                    {
                        continue;
                    }
                    newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, grid.GetNeighbours(currentNode)[x]);
                    if (newMovementCostToNeighbour < grid.GetNeighbours(currentNode)[x].gCost || !openSet.Contains(grid.GetNeighbours(currentNode)[x]))
                    {
                        grid.GetNeighbours(currentNode)[x].gCost = newMovementCostToNeighbour;
                        grid.GetNeighbours(currentNode)[x].hCost = GetDistance(grid.GetNeighbours(currentNode)[x], targetNode);
                        grid.GetNeighbours(currentNode)[x].parent = currentNode;
                        if (!openSet.Contains(grid.GetNeighbours(currentNode)[x]))
                        {
                            openSet.Add(grid.GetNeighbours(currentNode)[x]);
                        }
                        else
                        {
                            openSet.UpdateItem(grid.GetNeighbours(currentNode)[x]);
                        }
                    }
                }
            }
        }
        yield return null;
        if (pathSuccess)
        {
            wayPoints = RetracePath(startNode, targetNode);
        }
        else
        {

        }
        PathRequestManager.Instance.FinishedProcessingPath(wayPoints, pathSuccess);
    }
    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        path = new List<Node>();
        currentNode = endNode;
        while(currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        newWayPoints = SimplifyPath(path);
        Array.Reverse(newWayPoints);
        return (newWayPoints);
    }
    Vector3[] SimplifyPath(List<Node> path)
    {
        tempWayPoints = new List<Vector3>();
        directionOld = Vector2.zero;
        for(int x = 1; x < path.Count; x++)
        {
            directionNew = Vector2.right * (path[x - 1].gridX - path[x].gridX) + Vector2.up * (path[x - 1].gridY - path[x].gridY);
            if (directionNew != directionOld)
            {
                tempWayPoints.Add(path[x - 1].worldPosition);
            }
            directionOld = directionNew;
        }
        return tempWayPoints.ToArray();
    }
    int GetDistance(Node nodeA, Node nodeB)
    {
        distanceX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        distanceY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        if(distanceX > distanceY)
        {
            return 14 * distanceY + 10 * (distanceX - distanceY);
        }
        return 14 * distanceX + 10 * (distanceY - distanceX);
    }
}
