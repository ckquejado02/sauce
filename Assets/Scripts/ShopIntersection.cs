using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopIntersection : MonoBehaviour
{
    public static ShopIntersection Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
