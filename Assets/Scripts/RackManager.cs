using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RackManager : MonoBehaviour
{
    public static RackManager Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
