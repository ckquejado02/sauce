using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedPeopleContainer : MonoBehaviour
{
    public static SpawnedPeopleContainer Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
